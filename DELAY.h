/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */


/* Preprocessor Guard                                       */
#ifndef   DELAY_H_
#define   DELAY_H_

void delay_milliseconds (u32 Copy_u32OuterLoopIterations);


#endif
