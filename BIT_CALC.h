/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */


/* Preprocessor Guard                                       */
#ifndef BIT_CALC_H_
#define BIT_CALC_H_


#define BYTE_HIGH		(u8)0xFF

#define BYTE_LOW		(u8)0x00


/* This macro defines the low nibble of a 8-bit number */
#define LOW_NIBBLE		(u8)0x0F


/* This macro defines the high nibble of a 8-bit number */
#define HIGH_NIBBLE 	(u8)0xF0


/* ******************************************************************** */
/* This macro is used for setting a certain bit in a specific variable  */
/* Inputs: var: 	Variable to be changed							                  */
/*		   bitNo:	Number of bit to be set							                    */
/* ******************************************************************** */
#define SET_BIT(var,bitNo) ((var) |=  (1<<(bitNo)))


/* ******************************************************************** */
/* This macro is used for clearing a certain bit in a specific variable */
/* Inputs: var: 	Variable to be changed							                  */
/*		   bitNo:	Number of bit to be cleared						                  */
/* ******************************************************************** */
#define CLR_BIT(var,bitNo) ((var) &= ~(1<<(bitNo)))


/* ******************************************************************** */
/* This macro is used for toggling a certain bit in a specific variable */
/* Inputs: var: 	Variable to be changed							                  */
/*		   bitNo:	Number of bit to be toggled						                  */
/* ******************************************************************** */
#define TOG_BIT(var,bitNo) ((var) ^=  (1<<(bitNo)))


/* ***************************************************************************************** */
/* This macro is used for assigning a specific value to a certain bit in a specific variable */
/* Inputs: var: 	Variable to be changed							    					                         */
/*		   bitNo:	Number of bit to be toggled						    					                         */
/* ***************************************************************************************** */
#define ASG_BIT(var,bitNo,value) ((value)==1?SET_BIT(var,bitNo):CLR_BIT(var,bitNo))


/* ********************************************************************************* */
/* This macro is used for getting the value of a certain bit in a specific variable  */
/* Inputs: var: 	Variable to be changed							    			                     */
/*		   bitNo:	Number of bit to be get					    					                       */
/* ********************************************************************************* */
#define GET_BIT(var,bitNo)		 ((var&(1<<(bitNo)))>>(bitNo))


/* ********************************************************************* */
/* This macro is used for setting the high nibble of a specific variable */
/* Inputs: var: 	Variable to be changed								                 */
/* ********************************************************************* */
#define SET_HIGH_NIBBLE(var)	((var) |= (HIGH_NIBBLE))


/* ********************************************************************* */
/* This macro is used for setting the low nibble of a specific variable  */
/* Inputs: var: 	Variable to be changed								                 */
/* ********************************************************************* */
#define SET_LOW_NIBBLE(var)		((var) |= (LOW_NIBBLE))


/* ********************************************************************** */
/* This macro is used for clearing the high nibble of a specific variable */
/* Inputs: var: 	Variable to be changed								                  */
/* ********************************************************************** */
#define CLR_HIGH_NIBBLE(var)	((var) &= ~(HIGH_NIBBLE))


/* ********************************************************************** */
/* This macro is used for clearing the high nibble of a specific variable */
/* Inputs: var: 	Variable to be changed								                  */
/* ********************************************************************** */
#define CLR_LOW_NIBBLE(var)		((var) &= ~(LOW_NIBBLE))


/* ********************************************************************** */
/* This macro is used for toggling the high nibble of a specific variable */
/* Inputs: var: 	Variable to be changed								                  */
/* ********************************************************************** */
#define TOG_HIGH_NIBBLE(var)	((var) ^= (HIGH_NIBBLE))


/* ********************************************************************** */
/* This macro is used for toggling the low nibble of a specific variable  */
/* Inputs: var: 	Variable to be changed								                  */
/* ********************************************************************** */
#define TOG_LOW_NIBBLE(var)		((var) ^= (LOW_NIBBLE))


/* ************************************************************************************** */
/* This macro is used for right-circulating a number with a specific number of shifts 	  */
/* Inputs: var:			Variable to be changed											                          */
/*		   shiftNo: 	Number of shifts												                              */
/* ************************************************************************************** */
#define CIR_SHIFT_R8(var,shiftNo) ((var) = ((var)>>(shiftNo)) | ((var)<<(8-(shiftNo))))


/* ************************************************************************************** */
/* This macro is used for left-circulating a number with a specific number of shifts 	    */
/* Inputs: var:			Variable to be changed											                          */
/*	       shiftNo: 	Number of shifts												                            */
/* ************************************************************************************** */
#define CIR_SHIFT_L8(var,shiftNo) ((var) = ((var)<<(shiftNo)) | ((var)>>(8-(shiftNo))))



#define SET_BYTE(var)         			((var) = (BYTE_HIGH))

#define CLR_BYTE(var)         			((var) = (BYTE_LOW))

#define ASG_BYTE(var,value)				((var) = (value))

#define GET_BYTE(var)					((var))


#define CONC_8BIT(b7,b6,b5,b4,b3,b2,b1,b0)      CONC_HELPER(b7,b6,b5,b4,b3,b2,b1,b0)

#define CONC_HELPER(b7,b6,b5,b4,b3,b2,b1,b0)    0b##b7##b6##b5##b4##b3##b2##b1##b0



#endif
