/* ******************************************************** */
/*                                                          */
/*   Author  :          Hossam AbdulMageed                  */
/*   Date    :          February 7th, 2019                  */
/*   Version :          V01                                 */
/*                                                          */
/* ******************************************************** */

#include "STD_TYPES.h"
#include "BIT_CALC.h"

#define   INNER_LOOP_ITERATIONS       208




void delay_milliseconds (u32 Copy_u32OuterLoopIterations)
{

	u32 u32OuterLoopIndex = 0, u32InnerLoopIndex = 0;

	for(u32OuterLoopIndex = 0; u32OuterLoopIndex < Copy_u32OuterLoopIterations; u32OuterLoopIndex++)
	{

		// This loop yields 1 millisecond

		for(u32InnerLoopIndex = 0; u32InnerLoopIndex < INNER_LOOP_ITERATIONS; u32InnerLoopIndex++)
		{
			asm("NOP");
			asm("NOP");
			asm("NOP");
			asm("NOP");
		}
	}

}
